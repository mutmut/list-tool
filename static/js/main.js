

var Main = React.createClass({
	getInitialState: function() {
		return {elements: null, input: ''}
	},
	componentDidMount: function() {
		this.serialize()
	},
	serialize: function() {
		var bullet = JSON.stringify(this.state.elements);
		console.log(bullet)
		$.ajax({
			url: this.props.url,
			datatype: 'json',
			contentType: "string",
			type: 'POST',
			data: bullet,
			success: function(data) {
				if (JSON.parse(data) === null) {
					return
				} else {
					this.setState({elements: JSON.parse(data), input: ''})
				}
            }.bind(this)
		});
	},
	handleChange: function(e) {
		this.setState({input: e.target.value})
	},
	addBulletPoint: function(e, input) {
		e.preventDefault()
		if (this.state.elements === null) {
			this.state.elements = [[input, []]]
		} else {
			this.state.elements.push([input, []])
		}
		this.serialize()
	},
	addPoint: function(e, bulletIndex, input) {
		e.preventDefault()
		this.state.elements[bulletIndex][1].push([input, []])
		this.serialize()
	},
	addComment: function(e, bulletIndex, pointIndex, input) {
		e.preventDefault()
		this.state.elements[bulletIndex][1][pointIndex][1].push(input)
		this.serialize()
	},
	removeBullet: function(e) {
		e.preventDefault()
		this.state.elements.splice(e.target.id, 1)
		this.serialize()
	},
	removePoint: function(e, bulletIndex) {
		e.preventDefault()
		this.state.elements[bulletIndex][1].splice(e.target.id, 1)
		this.serialize()
	},
	removeComment: function(e, bulletIndex, pointIndex) {
		e.preventDefault()
		console.log(e.target.id)
		console.log(bulletIndex)
		console.log(pointIndex)
		this.state.elements[bulletIndex][1][pointIndex][1].splice(e.target.id, 1)
		this.serialize()
	},
	render: function() {
		return (
			<div className="container">
				<h1 className="title">Perseverance Race notes</h1>
				<BulletPoint elements={this.state.elements} addBulletPoint={this.addBulletPoint} addPoint={this.addPoint} addComment={this.addComment} removeBullet={this.removeBullet} removePoint={this.removePoint} removeComment={this.removeComment}/>
			</div>
		)	
	}
});

var BulletPoint = React.createClass({
	getInitialState: function() {
		return {input: ""}
	},
	handleChange: function(e) {
		this.setState({input: e.target.value})
	},
	addBulletPoint: function(e) {
		this.props.addBulletPoint(e, this.state.input)
		this.setState({input:""})
	},
	addPoint: function(e, bulletIndex, input) {
		this.props.addPoint(e, bulletIndex, input)
	},
	addComment: function(e, bulletIndex, pointIndex, input) {
		this.props.addComment(e, bulletIndex, pointIndex, input)
	},
	removeBullet: function(e) {
		this.props.removeBullet(e)
	},
	removePoint: function(e, pointIndex) {
		this.props.removePoint(e, pointIndex)
	},
	removeComment: function(e, bulletIndex, pointIndex) {
		this.props.removeComment(e, bulletIndex, pointIndex)
	},
	render: function() {
		return (
			<ul className="bulletpoint-list">
			{(() => {
				if (!(this.props.elements === null)) {
					return this.props.elements.map(function(item, i) {
						return (
							<li key={i} className="bulletpoint-container">
								<div className="button-container">
									<h3 className="bulletpoint">{item[0]}</h3>
									<img className="exit-icon" src="/static/img/exit.png" onClick={this.removeBullet} id={i}/>
								</div>
								<Point subpoints={item[1]} addPoint={this.addPoint} addComment={this.addComment} index={i} removePoint={this.removePoint} removeComment={this.removeComment}/>
							</li>
						)
					}, this)
				} else
					return ""
  			})()}
  			<form className="add-bullet-form" onSubmit={this.addBulletPoint}>
				<input className="add-bullet-textbox" type="text" placeholder="Add a BulletPoint" value={this.state.input} onChange={this.handleChange}/>
				<input className="add-bullet-button" type="button" value="+" onClick={this.addBulletPoint} />
			</form>
			</ul>
		)
	}

});
var Point = React.createClass({
	getInitialState: function() {
		return {input: ''}
	},
	handleChange: function(e) {
		this.setState({input: e.target.value})
	},
	addPoint: function(e) {
		this.props.addPoint(e, this.props.index, this.state.input) 
		this.setState({input: ""})
	},
	addComment: function(e, pointIndex, input) {
		this.props.addComment(e, this.props.index, pointIndex, input)
	},
	removePoint: function(e) {
		this.props.removePoint(e, this.props.index)
	},
	removeComment: function(e, pointIndex) {
		this.props.removeComment(e, this.props.index, pointIndex)
	},
	render: function() {
		return (
			<ul className="point-list">
				{(() => {
					if (this.props.subpoints.length > 0) {
						return this.props.subpoints.map(function(item, i) {
							return (
								<li key={i} className="point-container">
									<div className="button-container">
										<h3 className="point">{item[0]}</h3>
										<img className="exit-icon" src="/static/img/exit.png" onClick={this.removePoint} id={i}/>
									</div>
									<Comment comments={item[1]} addComment={this.addComment} index={i} removeComment={this.removeComment}/>
								</li>
							)
						}, this)
					} else
						return ""
  				})()}
  			<form className="add-point-form" onSubmit={this.addPoint}>
				<input className="add-point-textbox" type="text" placeholder="Add a Point" value={this.state.input} onChange={this.handleChange}/>
				<input className="add-point-button" type="button" value="+" onClick={this.addPoint} />
			</form>
			</ul>
		)
	}
});

var Comment = React.createClass({
	getInitialState: function() {
		return {input: ''}
	},
	handleChange: function(e) {
		this.setState({input: e.target.value})
	},
	addComment: function(e) {	
		this.props.addComment(e, this.props.index, this.state.input)
		this.setState({input: ""})
	},
	removeComment: function(e) {
		this.props.removeComment(e, this.props.index)
	},
	render: function() {
		return (
			<ul className="comment-list">
				{(() => {
					if (this.props.comments.length > 0) {
						return this.props.comments.map(function(item, i) {
							return (
								<div className="button-container">
									<li key={i} className="comment">{item}</li>
									<img className="comment-exit-icon" src="/static/img/exit.png" onClick={this.removeComment} id={i}/>
								</div>
							)
						}, this)
					} else
						return ""
  				})()}
  				<form className="add-comment-form" onSubmit={this.addComment}>
					<input className="add-comment-textbox" type="text" placeholder="Add a Comment" value={this.state.input} onChange={this.handleChange}/>
					<input className="add-comment-button" type="button" value="+" onClick={this.addComment}/>
				</form>
			</ul>
		)
	}
});

ReactDOM.render((
	<Main />
	), document.getElementById('content'))