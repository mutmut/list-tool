import json
import os
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def main():
	if request.method == 'POST': #needs to read JSON file
		print json.loads(request.data)
		if os.path.exists("list.json") and json.loads(request.data) == None:
			with open("list.json", 'r') as f:
				data_list = json.load(f)
			return json.dumps(data_list)
		elif not os.path.exists("list.json") and json.loads(request.data) == None:
			return "null"
		else:
			data_list = json.loads(request.data)
			print data_list
			with open('list.json', 'w') as f:
				f.write(json.dumps(data_list, indent=4))
				return json.dumps(data_list)
	return render_template('main.html')

if __name__ == "__main__":
    app.run(debug=True)